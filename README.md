## about
This meditation timer has customizable features beyond the time duration of your meditation session: there are three options for the Tibetan Bell sound and there is also the option to have reminders sounds along your meditation. In the end, by default, three bells are played.

Some features are still a work in progress, such as: saving all the time you meditated and a gamification for milestones in your total time meditated.

You can use the app [here.](https://meditate.elenavolpato.me/) 

Check out [my page](https://www.elenavolpato.me/) for more informayion about my work. I write about my learning process and my projects.


###  credits  

This app was made using Vite, Vue3, Vuex and tailwind CSS. 

I also used [NoSleep.js](https://github.com/richtr/NoSleep.js/) library and [persistedstate](https://reposhub.com/vuejs/vuex-utilities/robinvdvleuten-vuex-persistedstate.html) plugin.

The bells are all from: https://freesound.org/

Background animation by [deSandro](https://codepen.io/desandro/pen/BzJkQv). 
